
#include <iostream>

class Animal
{
public:
    virtual void Voice()
    {}
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow..\n";
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Mouuuu\n";
    }
};

int main()
{
    Animal* array[3];
    array[0] = new Dog;
    array[1] = new Cat;
    array[2] = new Cow;

    for (auto & i : array)
    {
        i->Voice();
    }
}
